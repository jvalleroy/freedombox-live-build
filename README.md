# Live Build Configuration for FreedomBox

This builds a FreedomBox live image. It requires the package `live-build`.

> **_NOTE:_** This is an experimental image configuration for
> demonstration and testing purposes. For normal FreedomBox use,
> please get an image from the [FreedomBox
> website](https://freedombox.org/).

## Building

	$ sudo lb build 2>&1 | tee build.log

## To-Dos

- [ ] DNS is not working in the image.
- [ ] Use `freedombox` package from buster-backports.
- [ ] Touch `/var/lib/freedombox/is-freedombox-disk-image` before
  `freedombox` package in installed. That way we can avoid needing to
  remove `/var/lib/plinth/firstboot-wizard-secret`.
- [ ] Look into read-write persistent storage.

## Useful Links

* [Debian Live Manual](https://live-team.pages.debian.net/live-manual/)
* [FreedomBox](https://freedombox.org/)
